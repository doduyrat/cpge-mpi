---
author: Mireille Coilhac
title: Suites - généralités
tags:
    - suites
---


## I. Définitions

!!! info "Une suite"

    * Une suite est une **liste** de nombres réels numérotés par les entiers naturels. On la note généralement $(u_n)$.
    * On appelle $n$ l’indice (ou rang) et $u_n$ est le terme d’indice $n$ de la suite $(u_n)$.
    * Le terme initial ou premier terme de la suite est soit $u_0$, soit $u_1$ selon les exercices.

!!! info "Notations"

    * On note une suite entre parenthèses, soit $(u_n)$ ou parfois $(u_n)_{n \in \mathbb{N}}$.
    * $u_n$ est le terme d'indice $n$ de la suite $(u_n)$.
    * Ainsi, $u_{n-1}$ est le terme qui précède $u_n$, et $u_{n+1}$ est le terme qui suit $u_n$. 

!!! info "Représentation graphique"

    
    On peut représenter une suite sur un graphique en mettant les valeurs de $n$ en abscisse et les valeurs correspondantes de $u_n$ en ordonnée.

    Il est très utile de savoir tracer la représentation graphique et obtenir le tableau de valeurs d'une suite à l'aide de la calculatrice.


## II. Mode de génération d'une suite

!!! info "Deux modes de génération"

    Une suite peut être définie  :

    ??? note "de manière explicite"

        Par une formule du type $u_n=f(n)$  

    ??? note "par récurrence"

        À l'aide d'une formule du type $u_{n+1}=f \left( u_n \right)$ (de proche en proche).


## III. Comportement Comportement global d'une suite

!!! info "Définitions"

    * On dit que la suite $(u_n)$ est croissante si pour tout entier $n$, $u_n \leq u_{n+1}$.
	
	* La suite est dite strictement croissante si l'inégalité est toujours stricte.
	
    * On dit que la suite $(u_n)$ est décroissante si pour tout entier $n$, $u_n \geq u_{n+1}$.
	
	* La suite est dite strictement décroissante si l'inégalité est toujours stricte.

    



## Crédits

Merci à Cédric Pierquet dont j'ai repris une grande partie du cours pour écrire cette page.