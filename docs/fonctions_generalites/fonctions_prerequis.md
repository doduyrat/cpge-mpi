---
author: Mireille Coilhac
title: Fonctions - prérequis
tags:
    - fonction
---

## Testez-vous

[Généralités sur les fonctions - 1](https://coopmaths.fr/alea/?uuid=77d18&id=3F1-act&n=10&d=10&s=2&alea=lVf9&cd=1&uuid=0eecd&id=3F10-1&n=3&d=10&s=3&alea=3swO&cd=1&uuid=c9382&id=3F10-5&n=8&d=10&s=3&s2=3&s3=5&alea=tl27&cd=1&uuid=ba520&id=3F10-2&n=6&d=10&s=5&s2=3&s3=5&alea=a99x&cd=1&uuid=4daef&id=3F10-3&n=10&d=10&s=5&alea=4DjD&cd=1&uuid=8a78e&id=3F10-6&alea=ogZQ&uuid=28997&id=2F20-4&n=1&d=10&s=3&s2=8&s3=false&alea=uBOS&cd=1&uuid=277d3&id=2F12-2&n=3&d=10&s=4&alea=9SJJ&cd=1&v=eleve&es=0111001&title=){ .md-button target="_blank" rel="noopener" }

## Vocabulaire sur les fonctions

