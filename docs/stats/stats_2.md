---
author: Mireille Coilhac
title: Statistiques à deux variables
tags:
    - statistiques
---

## Usage de la calculatrice

!!! info "Différentes calculatrices"

    [CASIO](a_telecharger/calc_statistique_a_deux_variables_casio.pdf){ .md-button target="_blank" rel="noopener" }

    [Numworks](a_telecharger/calc_statistique_a_deux_variables_numworks.pdf){ .md-button target="_blank" rel="noopener" }

    [Texas](a_telecharger/calc_statistique_a_deux_variables_ti83.pdf){ .md-button target="_blank" rel="noopener" }

